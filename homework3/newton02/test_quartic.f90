! $UWHPSC/codes/fortran/newton/test1.f90

program test_quartic

    use newton, only: solve, tol
    use functions, only: epsilonn, f_quartic, fprime_quartic

    implicit none
    real(kind=8) :: x, x0, fx, xstar
    real(kind=8) :: x0valsi(2), tolVals(3), epsVals(3)
    integer :: iters, itest1, itest2
	logical :: debug         ! set to .true. or .false.

    x0 = 4.d0
	print 10, x0
10  format("Starting with initial guess ", es22.15,/)
	debug = .false.
    
	! Values to test as epsilon:
    epsVals = (/1.d-4,1.d-8,1.d-12/)

	! Values to test as tol:
	tolVals = (/1.d-5,1.d-10,1.d-14/)
    print *, '    epsilon        tol    iters          x                 f(x)        x-xstar'
    
	do itest1 = 1,3

	    epsilonn = epsVals(itest1)
		xstar = 1.d0 + epsilonn**(0.25d0)

	    do itest2=1,3
		   
		    tol = tolVals(itest2)
            call solve(f_quartic, fprime_quartic,x0, x, iters, debug)
           
			fx = f_quartic(x)

	    	print 11, epsilonn, tol, iters, x, fx, x-xstar
11          format(2es13.3,i4,es24.15, 2es13.3)

        enddo          !itest2
       print*, ''
enddo             !itest1

end program test_quartic
