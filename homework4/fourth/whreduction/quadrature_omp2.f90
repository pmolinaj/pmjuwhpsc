module quadrature_omp2
    
      use omp_lib
      contains

      real(kind=8) function trapezoid(f,a,b,n)

          ! Estimate the integral of f(x) from a to b
          ! using the Trapezoid Rule with n points.

          ! Input:
          !   f: the function to integrate
          !   a: left endpoint
          !   b. right endpoint
          !   n: number of points to use
          ! Returns:
          !   the estimate of the integral
          implicit none

          real(kind=8), intent(in) :: a,b
          real(kind=8), external :: f
          integer, intent(in) :: n

          !local variables
          integer :: j
          real(kind=8) :: h, trap_sum, xj, mysum
          

          h = (b-a) / (n-1)
          trap_sum = .5d0 * ( f(a) + f(b) ) ! endpoint contributions)

          !$omp parallel private(mysum) shared(trap_sum)
          mysum = 0.d0

          !$omp do 
          do j = 2, n-1
              xj = a + (j - 1) * h
              mysum = mysum + f(xj)
          enddo

          !$omp critical
          trap_sum = trap_sum + mysum
          !$omp end critical
          !$omp end parallel

          trapezoid = h * trap_sum
         
      end function trapezoid

      subroutine error_table(f,a,b,nvals,int_true)

          implicit none
          real(kind=8), intent(in) :: a, b, int_true
          real(kind=8), external ::  f
          integer, dimension(:), intent(in) :: nvals

          ! local variables
          integer :: j, n
          real(kind=8) :: int_trap, error, last_error, ratio 

          print *, "    n         trapezoid            error       ratio"

          last_error = 0.d0
          
          do j = 1, size(nvals)
              n = nvals(j)
              int_trap =  trapezoid(f,a,b,n)
              error = abs(int_trap - int_true)
              ratio = last_error / error
              last_error = error ! for next n 

              print 11, n, int_trap, error, ratio
11            format(i15, es22.14, es13.3, es13.3)
          enddo

      end subroutine error_table

end module quadrature_omp2 
