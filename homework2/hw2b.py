
"""
Demonstration module for quadratic interpolation.
Update this docstring to describe your code.
Modified by: ** Pedro Molina **
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:

    A = np.vstack([np.ones(3),xi,xi**2]).T
    c = solve(A,yi)

    return c

def cubic_interp(xi,yi):
    """
    Cubic Interpolation. A function that solve the interpolation problem
    when the input, xi and yi are of length 4. The solution is determining by
    the cubic polynomial: p(x) = c0 + c1x + c2x**2 + c3 x**3 that
    interpolates 4 points. This requires solving a linear systems of 4
    equations for the 4 unknown coefficients.
    """
    # check inputs and print error message if not valid:
      
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message
     
    error_message = "xi and yi should have length 3"
    assert len(xi)==4 and len(yi)==4, error_message
            
    # Set up linear system to interpolate through data points:
      
    A = np.vstack([np.ones(4),xi,xi**2,xi**3]).T
    c = solve(A,yi)
     
    return c

def poly_interp(xi,yi):
    """
    Generalization of the above function, to accept arrays xi, and yi
    of any length n.
    """

    # check inputs and print error message if not valid:
    nlen = len(xi)
    nylen = len(yi)

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have the length %s" % nlen
    assert len(xi) == nylen and len(yi) == nlen, error_message

    # Set up linear system to interpolate through data points:
    A = np.vstack([xi**j for j in range(nlen)]).T
    c = solve(A,yi)

    return c

def plot_poly(xi,yi):
    pass


def plot_quad(xi,yi,debug = False):
    """
    Given the coordinates x,y of three points, calculate
    the values of the coeficients of th  polynomials 
    p(x) = c0 + c1 x + c2 x**2  that fits the  three 
    points, represent the polynomial, and the three points
    """

    c = quad_interp(xi,yi)
    if debug == True:
        print xi
        print yi
        # print "xi es %8.3f e yi es %8.3f" %(xi,yi)
        #print "Coeficientes del polinomio = %8.3f" %c
        print c
        #print  x
    
    x = np.linspace(xi.min()-1,xi.max() + 1,1000)
    y = c[0] + c[1]*x + c[2]*x**2

    plt.figure(1)      # open plot figure window
    plt.clf()          # clear figure
    plt.plot(x,y,'b-') # conect points with a blue line

    # Add data points (polynomial shoulg go through these points!)
    plt.plot(xi,yi,'ro')     # plot as red circles
    plt.ylim(y.min() - 1, y.max() + 1.)    # set limit in y for plot

    plt.title("Data points and interpolating polynomial")

    plt.savefig('quadratic.png')  # save figure as .png file

def plot_cubic(xi,yi,debug=False):
    """
    Given the coordinates x,y of three points, calculate
    the values of the coeficients of th  polynomials 
    p(x) = c0 + c1 x + c2 x**2  that fits the  three 
    points, represent the polynomial, and the three points
    """

    c = cubic_interp(xi,yi)
    if debug == True:
        print xi
        print yi
        # print "xi es %8.3f e yi es %8.3f" %(xi,yi)
        #print "Coeficientes del polinomio = %8.3f" %c
        print c
        #print  x

    x = np.linspace(xi.min()-1,xi.max() + 1,1000)
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3

    plt.figure(1)      # open plot figure window
    plt.clf()          # clear figure
    plt.plot(x,y,'b-') # conect points with a blue line

    # Add data points (polynomial shoulg go through these points!)
    plt.plot(xi,yi,'ro')     # plot as red circles
    plt.ylim(y.min() - 1, y.max() + 1.)    # set limit in y for plot
    plt.title("Data points and interpolating polynomial")

    plt.savefig('cubic.png')  # save figure as .png file

def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
            "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)


def test_quad2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([1.,  2.,  3.])
    yi = np.array([ 1., -1.,  1.])
    c = quad_interp(xi,yi)
    c_true = np.array([7., -8.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
    "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
    
def test_cubic():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array( [1.,2.,3.,4.] )
    yi = np.array( [2.,1.,2.,1.] )
    c = cubic_interp(xi,yi)
    c_true = np.array([9.0,-11.333333333333336,5.0,-0.66666666666666674])
    print "c      = ", c
    print "C_true = ", c_true
    #test that all elements have small error:
    assert np.allclose(c,c_true), \
            "Incorrect result, c = %s, Expected: c = %s" % (c, c_true)

def plot_poly(xi, yi):
    """
    Perform polynomial interpolation and plot the resulting function along
    with the data points.
    """

    # Compute the coefficients:
    c = poly_interp(xi,yi)

    # Plot the resulting polynomial:
    #x = np.linspace(xi.min() - 1,  xi.max() + 1, 1000)
    x = np.linspace(xi.min(),  xi.max(), 1000)

    # Use Horner's rule:
    n = len(xi)
    y = c[n-1]
    for j in range(n-1, 0, -1):
        y = y*x + c[j-1]

    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line

    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles
    plt.ylim(y.min()-1, y.max()+1)       # set limits in y for plot

    plt.title("Data points and interpolating polynomial")

    plt.savefig('poly.png')   # save figure as .png file
        
if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    test_quad2()
    test_cubic()

