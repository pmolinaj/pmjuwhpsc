program test1pmj

    use quadrature , only: f, analInt, trapezoid, error_table

    implicit none
    real(kind=8) :: AA,BB , int_true
    integer :: nvals(7), i

    AA = 0.d0
    BB = 2.d0

     int_true = analInt(AA,BB)

    print 10, int_true
10  format("true integral: ", es22.14)
    print *, " " ! blank line

    ! values of n to test:
    do i = 1, 7
        nvals(i) = 5 * 2**(i-1)
    end do 

    call error_table(f,AA,BB,nvals)
    
end program test1pmj
