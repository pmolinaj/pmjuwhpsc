! $UWHPSC/codes/fortran/newton/functions.f90

module functions
		implicit none
		real(kind=8) :: pi
		real(kind=8) :: epsilonn
		save

contains

real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt


real(kind=8) function g1fun(x)
    implicit none
    real(kind=8), intent(in) :: x

    g1fun =  1.d0 - 0.6d0*x**2

end function g1fun

real(kind=8) function g2fun(x)
		implicit none
		real(kind=8), intent(in) :: x
		pi = acos(-1.d0)

		g2fun = x*cos(pi*x)

end function g2fun

real(kind=8) function g1prime(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    g1prime = -1.2d0*x 

end function g1prime

real(kind=8) function g2prime(x)
		implicit none
		real(kind=8), intent(in) :: x
		pi = acos(-1.d0)

		g2prime = cos(pi*x) - x*pi*sin(pi*x) + 1.2d0*x 

end function g2prime

real(kind=8) function fvals(x)
		implicit none
		real(kind=8), intent(in) :: x

		fvals = g1fun(x) - g2fun(x)

end function fvals

real(kind=8) function fpvals(x)
		implicit none
		real(kind=8), intent(in) :: x

		fpvals = g1prime(x) - g2prime(x)

end function fpvals

real(kind=8) function f_quartic(x)
		implicit none
		real(kind=8), intent(in) :: x

		f_quartic = (x - 1.d0)**4 - epsilonn


end function f_quartic

real(kind=8) function fprime_quartic(x)
		implicit none
		real(kind=8), intent(in) :: x

		fprime_quartic = 4.d0 * (x - 1.d0)**3

end function fprime_quartic


end module functions
