# $UWHPSC/codes/homework3/test_code.py 
# To include in newton.py
from newton import solve
from numpy import linspace, pi, cos, sin, abs, where
import matplotlib.pyplot as plt

def funcion1(x):
	g1 = sin(x)
	g1p = cos(x)
	return g1,g1p
	
def funcion2(x):
	g2 = 1. - x**2
	g2p = -2.*x
	return g2, g2p

	
def fvals(x):
	"""
	"""
	g1,g1p = funcion1(x)
	g2,g2p = funcion2(x)
	f = g1 - g2
	fp = g1p - g2p
	return f, fp
	
def intersec(xIni,plott=False):
	"""
	- Calcula los puntos de interseccion de dos funciones en 1D
	
	- Entrada:  En forma de tuplas
	            func1 : valores de f y fprima para la primera funcion
	            func2 : valores de f y fprima para la segunda funcion
	            funcInt : f1 - f2; f1p - f2p
	            xIni : valores iniciales aproximados.
	            plott: Condicional para dibujar:
	                    automatico, no grafica
	                    Fijado True, grafica, las dos funciones y los 
	                                  puntos de interseccion
	"""
	
	x0 = xIni
	if plott:
		xx = linspace(-10, 10, 1000)
		g1xx, g1pxx = funcion1(xx)
		g2xx, g2pxx = funcion2(xx)
		plt.figure(1)
		plt.clf()
		plt.plot(xx,g1xx, 'b')
		plt.plot(xx, g2xx, 'r')
		plt.legend(['g1','g2'])

	for x0 in xIni:
		x, iters = solve(fvals, x0)
		print "\nWith initial guess x0 = %22.4e, " % x0
		print "    solve returns x = %22.4e after %i iterations" % (x, iters)
		g1x,g1px = funcion1(x)
		fx,fpx = fvals(x)
		print "    f(x) =%22.4e" % fx
		if plott:
			plt.plot(x,g1x,'ko')
	
	xmin = -5
	xmax =  5
	ymin = -4
	ymax =  4 
	plt.axis((xmin,xmax,ymin,ymax))
	plt.savefig('intersection2function.png')
	
