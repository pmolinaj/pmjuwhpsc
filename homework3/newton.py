# $UWHPSC/codes/homework3/test_code.py 
# To include in newton.py



def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def solve(fvals,x0,debug=False):
    """
    Function that find the root of a nonlinear function f(x).
    Input: fvals, values of f(x) and f'(x), for any input of x.
    x0, the initial guess, debug, an optional argument with 
    default False, for debugging purposes.
    """
    tol = 1e-14
    maxiter = 20
    x=x0
    
    if debug:
		print 'Initial guess: x0 = %22.15e ' % x0

    # Newton iteration to find a zero of f(x)

    for i in xrange(0,maxiter):
         
        
        # evaluate function and its derivative
        fx, fxprime = fvals(x)

        if abs(fx) < tol:
            break

        # compute Newton increment x:
        deltax = fx/fxprime

        # update x:
        x = x - deltax
        #if abs(deltax / x) < tol:
        #    break
        if debug:
             print "After %s iteration, x = %22.15e" % (i + 1,x)
    
    if i == maxiter - 1:
		fx,fxprime = fvals(x)
		if abs(fx) > tol:
			print "*** warning: has not yet converged"
		return x, i+1
    else:
        return x, i


def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x
