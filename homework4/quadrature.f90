module quadrature

    contains

      real(kind=8) function analInt(a,b)
          
          implicit none
          real(kind=8), intent(in) :: a,b
          
          analInt = (b-a) + (b**4 - a**4) /4.0

      end function analInt

      real(kind=8) function f(x)
          
          implicit none
          real(kind=8), intent(in) :: x
      
          f = 1.d0 + x**3
          
      end function f

      real(kind=8) function trapezoid(f,a,b,n)
          
          implicit none

          real(kind=8), intent(in) :: a,b
          real(kind=8), external :: f
          integer, intent(in) :: n
          !local variables
          real(kind=8) :: h, xj , sum_f
          real(kind=8), dimension(n) :: fj
          real(kind=8) :: sum
          integer :: j,i

          h = (b-a) / (n-1)
          sum_f = 0.d0
          do j = 2, n-1
              xj = a + (j-1)*h
              ! fj(j) = f(xj)
              sum_f = sum_f + f(xj)
          end do

          trapezoid = h*(sum_f + 0.5*(f(a)+f(b)))
         
      end function trapezoid

      subroutine error_table(f,a,b,nvals)

          implicit none
          real(kind=8), intent(in) :: a,b
          real(kind=8), external ::  f
          integer, dimension(:), intent(in) :: nvals

          ! local variables
          real(kind=8) :: int_trap, int_true, error, last_error, ratio 
          integer :: i, len_h, nh

          print *, "    n         trapezoid            error       ratio"

          last_error = 0.d0

          len_h = size(nvals)
          
          do i =1,len_h
              nh = nvals(i)
              
              ! Use functions trapezoid and anal_int
              !  to calculate the analytical solution
              !  and the approximated solution of the
              !  integral of f(x)
              
              int_trap =  trapezoid(f,a,b,nh)
              int_true =  analInt(a,b)
              
              error = abs(int_trap - int_true)
              ratio = last_error / error
              last_error = error  

              print 11, nh, int_trap, error, ratio
11            format(i8, es22.14, es13.3, es13.3)
          enddo

      end subroutine error_table

end module quadrature 


