"""
Demonstration script for quadratic interpolation.

hw2a.py determine the quadratic polynomial 
p(x) = c0 + c1x +c2x^2 that passes throught 
three given data points (xi,yi) for i = 1,2,3.

In this case, the points are 
   (x1,y1) = (-1,0)
   (x2,y2) = ( 1,4)
   (x3,y3) = ( 2,3)

The system of equation to solve is
   
     c0 -  c1 +   c2 = 0
     c0 + 0c1 +  0c2 = -1
     c0 + 2c1 + 4c2  = 7

This has the form Ac=y where y is the vector data
c is the vector of coefficients we seek and A is a 
3 x 3 matrix.

    |1  -1   1|
A = |1   0   0|
    |1   2   4|
    
Modified by: ** Pedro Molina **
"""

import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

# Set up linear system to interpolate through data points:

# Data points:
xi = np.array([-1., 1., 2])
yi = np.array([ 0., 4., 3.])

# It would be better to define A in terms of the xi points.
# Doing this is part of the homework assignment.

#A = np.array([[1., -1., 1.], [1., 1., 1.], [1., 2., 4.]])

A = np.vstack([np.ones(3),xi,xi**2]).T

b = yi

# Solve the system:
c = solve(A,b)

print "The polynomial coefficients are:"
print c

# Plot the resulting polynomial:
x = np.linspace(-2,3,1001)   # points to evaluate polynomial
y = c[0] + c[1]*x + c[2]*x**2

plt.figure(1)       # open plot figure window
plt.clf()           # clear figure
plt.plot(x,y,'b-')  # connect points with a blue line

# Add data points  (polynomial should go through these points!)
plt.plot(xi,yi,'ro')   # plot as red circles
plt.ylim(-2,8)         # set limits in y for plot

plt.title("Data points and interpolating polynomial")

plt.savefig('demo1plot.png')   # save figure as .png file
